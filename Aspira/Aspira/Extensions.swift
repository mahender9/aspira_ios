//
//  Extensions.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func rounded() {
        self.layer.cornerRadius = self.frame.size.width * 0.5
        self.clipsToBounds = true
    }
}

extension UIView {
    
    func enableBorder() {
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 2.0
        self.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        self.clipsToBounds = true

    }
}
