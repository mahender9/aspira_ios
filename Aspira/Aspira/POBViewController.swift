//
//  POBViewController.swift
//  Aspira
//
//  Created by mahender.r.gurram on 11/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class POBViewController: UIViewController {
    
    private enum POBEnum : Int,CaseIterable {
        case retailername = 0
        case retailerstockistname
        case invoicenumber
    }
    private enum TotalSubmit : Int,CaseIterable {
          case total = 0
          case submit
      }
    var arrPOBTitles = ["Retailer Name","Retailer Stockist Name","Invoice Number"]
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpTableView()
        self.navigationItem.title = "POB"
    }
    
    func setUpTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(UINib(nibName: BrandDaysBaseCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: BrandDaysBaseCell().getIdentifier())
        tableView.register(UINib(nibName: SubmitButtonCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: SubmitButtonCell().getIdentifier())
        tableView.register(UINib(nibName: POBTotalTableViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: POBTotalTableViewCell().getIdentifier())
        tableView.register(UINib(nibName: SKUBaseTableViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: SKUBaseTableViewCell().getIdentifier())
        tableView.register(UINib(nibName: TotalPOBTableViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: TotalPOBTableViewCell().getIdentifier())

    }

    

}

extension POBViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch section {
        case 0: return 1
        case 1: return POBEnum.allCases.count
        case 2: return 3
        case 3: return TotalSubmit.allCases.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TotalPOBTableViewCell().getIdentifier(), for: indexPath) as? TotalPOBTableViewCell else { return UITableViewCell() }
            return cell

        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: BrandDaysBaseCell().getIdentifier(), for: indexPath) as? BrandDaysBaseCell else { return UITableViewCell() }
            cell.setData(title: arrPOBTitles[indexPath.row])
            cell.setPlaceholderValue(title: arrPOBTitles[indexPath.row])

            return cell

        case 2:
            
           guard let cell = tableView.dequeueReusableCell(withIdentifier: SKUBaseTableViewCell().getIdentifier(), for: indexPath) as? SKUBaseTableViewCell else { return UITableViewCell() }
           if indexPath.row == 2 { //array lastcount
            cell.buttonAddMore.isHidden = false
           }else {
            cell.buttonAddMore.isHidden = true
           }
            return cell

        case 3:
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: POBTotalTableViewCell().getIdentifier(), for: indexPath) as? POBTotalTableViewCell else { return UITableViewCell() }
                return cell

            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonCell().getIdentifier(), for: indexPath) as? SubmitButtonCell else { return UITableViewCell() }
                return cell

            }

        default:
            break
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: return TotalPOBTableViewCell().getCellHeight()

        case 1: return BrandDaysBaseCell().getCellHeight()

        case 2: return SKUBaseTableViewCell().getCellHeight()
    
        case 3:
            if indexPath.row == 0 {
                return POBTotalTableViewCell().getCellHeight()
            }
            return SubmitButtonCell().getCellHeight()

        default:
            break
        }
        return CGFloat.leastNormalMagnitude
    }
    
    
}
