//
//  BrandDaysBaseCell.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class BrandDaysBaseCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textFieldBaseView: UIView!
    @IBOutlet weak var textFieldInvisible: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldBaseView.enableBorder()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(title : String) {
        titleLabel.text = title
    }
    
    func setPlaceholderValue(title : String) {
        textFieldInvisible.placeholder = "Enter " + title
       }
    
    func getIdentifier() -> String {
        return "BrandDaysBaseCell"
    }
    
    func getCellHeight() -> CGFloat {
        return 75.0
    }

}
