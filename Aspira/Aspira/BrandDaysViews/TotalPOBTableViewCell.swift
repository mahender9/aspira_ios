//
//  TotalPOBTableViewCell.swift
//  Aspira
//
//  Created by mahender.r.gurram on 11/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class TotalPOBTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPOBfor: UILabel!
    @IBOutlet weak var labelPODDone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelPODDone.text = "Total no of POB done in Novermber:0"
        labelPOBfor.text = "Total value of POB for November:0.00"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getIdentifier() -> String {
        return "TotalPOBTableViewCell"
    }
    
    func getCellHeight() -> CGFloat {
        return 80.0
    }

    
    
}
