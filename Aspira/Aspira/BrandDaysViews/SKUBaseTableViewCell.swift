//
//  SKUBaseTableViewCell.swift
//  Aspira
//
//  Created by mahender.r.gurram on 11/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class SKUBaseTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBaseSKU: UIView!
    @IBOutlet weak var viewBasePTS: UIView!
    @IBOutlet weak var viewBaseQTY: UIView!
    
    @IBOutlet weak var textFieldBaseSKU: UITextField!
    @IBOutlet weak var textFieldBasePTS: UITextField!
    @IBOutlet weak var textFieldBaseQTY: UITextField!

    @IBOutlet weak var buttonAddMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBaseSKU.enableBorder()
        viewBasePTS.enableBorder()
        viewBaseQTY.enableBorder()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getIdentifier() -> String {
        return "SKUBaseTableViewCell"
    }
    
    func getCellHeight() -> CGFloat {
        return 100.0
    }
    func getCellHeightMin() -> CGFloat {
        return 100.0
    }
    
    @IBAction func actionAddMore(_ sender: Any) {
    }
    
}
