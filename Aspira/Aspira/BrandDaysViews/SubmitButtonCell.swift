//
//  SubmitButtonCell.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class SubmitButtonCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getIdentifier() -> String {
        return "SubmitButtonCell"
    }
    
    func getCellHeight() -> CGFloat {
           return 45.0
       }

}
