//
//  BrandDaysViewController.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class BrandDaysViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private enum BrandDaysEnum : Int,CaseIterable {
        case selectactivity = 0
        case doctorname
        case speciality
        case selectdate
    }
    private enum TotalSubmit : Int,CaseIterable {
          case total = 0
          case submit
      }
    
    var arrBrandDaysTitles = ["Select Activity","Doctor Name","Speciality","Select Date"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Brand Days"
        setUpTableView()

        // Do any additional setup after loading the view.
    }
    
    func setUpTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(UINib(nibName: BrandDaysBaseCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: BrandDaysBaseCell().getIdentifier())
        tableView.register(UINib(nibName: SubmitButtonCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: SubmitButtonCell().getIdentifier())
        tableView.register(UINib(nibName: POBTotalTableViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: POBTotalTableViewCell().getIdentifier())
        tableView.register(UINib(nibName: SKUBaseTableViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: SKUBaseTableViewCell().getIdentifier())


    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BrandDaysViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch section {
        case 0: return BrandDaysEnum.allCases.count
        case 1: return 3
        case 2: return TotalSubmit.allCases.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: BrandDaysBaseCell().getIdentifier(), for: indexPath) as? BrandDaysBaseCell else { return UITableViewCell() }
            cell.setData(title: arrBrandDaysTitles[indexPath.row])
            return cell

        case 1:
            
           guard let cell = tableView.dequeueReusableCell(withIdentifier: SKUBaseTableViewCell().getIdentifier(), for: indexPath) as? SKUBaseTableViewCell else { return UITableViewCell() }
           if indexPath.row == 2 { //array lastcount
            cell.buttonAddMore.isHidden = false
           }else {
            cell.buttonAddMore.isHidden = true
           }
            return cell

        case 2:
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: POBTotalTableViewCell().getIdentifier(), for: indexPath) as? POBTotalTableViewCell else { return UITableViewCell() }
                return cell

            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonCell().getIdentifier(), for: indexPath) as? SubmitButtonCell else { return UITableViewCell() }
                return cell

            }

        default:
            break
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: return BrandDaysBaseCell().getCellHeight()

        case 1:
            if indexPath.row == 2 { //array lastcount
                return SKUBaseTableViewCell().getCellHeight()
            }else {
                return SKUBaseTableViewCell().getCellHeightMin()
            }
    
        case 2:
            if indexPath.row == 0 {
                return POBTotalTableViewCell().getCellHeight()
            }
            return SubmitButtonCell().getCellHeight()

        default:
            break
        }
        return CGFloat.leastNormalMagnitude
    }
    
    
}
