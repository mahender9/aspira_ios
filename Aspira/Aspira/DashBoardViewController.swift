//
//  DashBoardViewController.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit
import LMCSideMenu

class DashBoardViewController: UIViewController, LMCSideMenuCenterControllerProtocol {

    var interactor: MenuTransitionInteractor = MenuTransitionInteractor()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSideMenu()
        // Do any additional setup after loading the view.
    }
    
    func setUpSideMenu() {
        //init left and right menu controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let leftMenuController = storyboard.instantiateViewController(withIdentifier: String(describing: LeftMenuController.self)) as! LeftMenuController
        //Setup menu
        setupMenu(leftMenu: leftMenuController, rightMenu: nil)
        
        //enable screen edge gestures if needed
        enableLeftMenuGesture()

    }
    
    @IBAction func actionLeftMenu(_ sender: Any) {
        presentLeftMenu()

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
