//
//  ProfileViewCell.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class ProfileViewCell: UITableViewCell {
    @IBOutlet weak var imageViewProfile: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewProfile.rounded()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getIdentifier() -> String {
        return "ProfileViewCell"
    }
    
}
