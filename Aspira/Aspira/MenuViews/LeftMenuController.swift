//
//  LeftMenuController.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class LeftMenuController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private enum MenuSections : Int,CaseIterable {
        case profile = 0
        case menu
        case others
    }
    
    var menuItems = ["My POB Value","My Inputs of the Month","My Comptetion","Brand Days","RxGA"]
    var otherMenuItems = ["Change Password","Rate Us","Contact Us","Log out"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        // Do any additional setup after loading the view.
    }
    
    func setUpTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(UINib(nibName: ProfileViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: ProfileViewCell().getIdentifier())
        tableView.register(UINib(nibName: MenuBaseTableViewCell().getIdentifier(), bundle: nil), forCellReuseIdentifier: MenuBaseTableViewCell().getIdentifier())

    }
    
}
extension LeftMenuController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return MenuSections.allCases.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch MenuSections(rawValue: section) {
            case .profile:  return ""
            case .menu : return ""
            case .others : return "Others"
            case .none:
            break
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch MenuSections(rawValue: section) {
            case .profile:  return 1
            case .menu : return 5
            case .others : return 4
            case .none:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch MenuSections(rawValue: indexPath.section) {
        case .profile:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileViewCell().getIdentifier(), for: indexPath) as? ProfileViewCell else { return UITableViewCell() }
            return cell
            
        case .menu :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuBaseTableViewCell().getIdentifier(), for: indexPath) as? MenuBaseTableViewCell else { return UITableViewCell() }
            cell.configureData(title: menuItems[indexPath.row], image: "")
            return cell
        case .others :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MenuBaseTableViewCell().getIdentifier(), for: indexPath) as? MenuBaseTableViewCell else { return UITableViewCell() }
            cell.configureData(title: otherMenuItems[indexPath.row], image: "")
            return cell

        case .none:
            break
        }
        return UITableViewCell()
    }
    
    
}
