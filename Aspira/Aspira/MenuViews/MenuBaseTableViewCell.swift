//
//  MenuBaseTableViewCell.swift
//  Aspira
//
//  Created by mahender.r.gurram on 10/11/19.
//  Copyright © 2019 Reddy's Labs. All rights reserved.
//

import UIKit

class MenuBaseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewMenu: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureData (title: String , image : String) {
        labelTitle.text = title
        imageViewMenu.image = UIImage(named: image)
    }
    
    func getIdentifier() -> String {
        return "MenuBaseTableViewCell"
    }

}
